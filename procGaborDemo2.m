% This demo illustrates two things:(a) The creation of a Gabor stimulus
% based on the ptb-3 command CreateProceduralGabor() and
% (b) how this (stationary) stimulus is animated by a (constant) phase shift
% on the PROPixx DLP projector with a vertical refresh rate of 1440 Hz.
% Theoretically, the grating should become invisible once its temporal
% frequency surpasses 60 Hz (Castet et al., 2002)...
%
% Jan Klanke 06/21
%
%
% 

close all
clear all
clc

try 
    HideCursor;
    ListenChar(2);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %       Define stuff for screen       %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % base screen parameter:
    set.pixx  = 1;            % are you using the propixx? 0= no, 1= yes
    set.plot  = 0;            % do you want to create a plot for the frame times
    
    % Initialize for unified KbName's and normalized 0 - 1 color range:
    PsychDefaultSetup(2);

    % If there are multiple displays guess that one without the menu bar is the
    % best choice.  Dislay 0 has the menu bar.
    scr.allScreens = Screen('Screens');
    scr.expScreen  = max(scr.allScreens);

    % specify color stuff:
    scr.bgLum = 0.5;
    scr.black = BlackIndex(scr.expScreen);
    scr.white = WhiteIndex(scr.expScreen);
    scr.gray  = GrayIndex(scr.expScreen);
    scr.bgColor = scr.black + (scr.bgLum * (scr.white - scr.black)); 
    scr.inColor = scr.white-scr.bgColor;       % increment for full contrast

    % define parameters for propixx setup:
    if set.pixx 

        % base parameter for our propixx setup:
        scr.rate  = 12;           % number of stimulus updates per refresh
        scr.subDist = 3400;       % subject distance [mm]
        scr.width = 2502;         % screen width [mm]

        % flipmethod:
        scr.flipmethod = 2; % Fastest method, but only available on Linux with open-source graphics drivers.

        % initialize data pixx connection for 1440-Hz display control (5).
        initDatapixx(5);

        % may or may not help: 
        PsychImaging('PrepareConfiguration');

        if scr.flipmethod >= 1
            % For drawing during async flip - aka effective triplebuffering -
            % to work, we need a virtual framebuffer. This also helps for 
            % flipmethod == 2 on Linux with double-buffering, because it
            % decouples swap completion aka availability of the backbuffer
            % from stimulus rendering and composition for 4 quadrant 3 RGB
            % channels, so those steps can run while a bufferswap is still
            % pending.
            PsychImaging('AddTask', 'General', 'UseVirtualFramebuffer');
        end

        % 16 bpc float is enough for a net 8 bpc output precision:
        PsychImaging('AddTask', 'General', 'FloatingPoint16Bit');

        % Open a window.  Note the new argument to OpenWindow with value 2,
        % specifying the number of buffers to the onscreen window.
        scr.main = PsychImaging('OpenWindow', scr.expScreen, 0.5);
        scr.textRenderer = Screen('Preference', 'TextRenderer',1);
        scr.textAntiAliasing = Screen('Preference', 'TextAntiAliasing',1);

        % Setup for fast display mode, producing the final image in onscreen window
        % 'main', for presentation at rate 'rate' (4 or 12), with 'flipmethod'.
        % Replace 0 with 1 for GPU load benchmarking - has some performance impact
        % itself, but allows assessment of how much we make the graphics card sweat:
        PsychProPixx('SetupFastDisplayMode', scr.main, scr.rate, scr.flipmethod, [], 0);

        % get a suitable offscreen window 'myimg' for drawing our stimulus:
        scr.myimg = PsychProPixx('GetImageBuffer');

        % add info on vertical refresh rate and frame duration:
        scr.refr = scr.rate * Screen('NominalFrameRate',scr.main); % screen refresh rate (Hz)
        scr.fd   = 1/scr.refr;      % frame duration (sec)

    % define parameters for non-propixx setup (i.e., this is just some dummy varibales
    % that work on my laptop--might need a bit of tweaking for other work stations):   
    else
        scr.subDist = 300;          % subject distance (mm)
        scr.width   = 285.0;        % width  of screen (mm) 
        scr.height  = 180.0;        % height of screen(mm)en (cm)
        scr.rate    = 1;            % number of stimulus updates per refresh
        scr.refr    = 60;           % screen refresh rate (Hz)
        scr.fd      = 1/scr.refr;      % frame duration (sec)

        % initialize for unified KbName's and normalized 0 - 1 color range:
        PsychDefaultSetup(2);

        % open the normal screen:
        [scr.main,scr.rect] = PsychImaging('OpenWindow', scr.expScreen, scr.bgColor);
        scr.myimg = scr.main; % for easy pickings; equate pixx buffer handle with screen image handle 

        % change font size:
        Screen('TextFont', scr.myimg,'-adobe-helvetica-medium-o-normal--25-180-100-100-p-130-iso8859-1');
    end 

    % determine th main window's center:
    [scr.centerX, scr.centerY] = WindowCenter(scr.myimg);
    [scr.resX   , scr.resY   ] = WindowSize(scr.myimg);
    scr.rect = [0 0 scr.resX scr.resY];

    % dva2pix:
    scr.ppd = scr.subDist*tan(pi/180)/(scr.width/scr.resX);	 % [pix / 1 dav]

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Define stuff for mouse and keyboard %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    upKey = KbName('UpArrow');
    downKey = KbName('DownArrow');
    spaceKey = KbName('Space');
    escapeKey = KbName('ESCAPE');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %          Define grating           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % define parameters of the grating:
    par.amp  = 1;   % contrast amplitude of the grating
    par.ori  = 180; % grating orientation
    par.pha  = 0;   % phase ondry [deg]
    par.frq  = 1;   % spatial frequency of the grating in [cpd]
    par.sig  = 5/4; % std.dev. of Gaussian envelope
    par.siz  = 10;  % diameter of the stimulus [deg]
    par.asp  = 1;   % aspect ratio

    par.frqp = par.frq/scr.ppd;            % spatial frequency of the grating in [cpp]
    par.sigp = par.sig*scr.ppd;            % std.dev. of Gaussian envelope [pix]
    par.sizp = ceil(par.siz*scr.ppd);      % size of the grating [pix]
    par.sizp = par.sizp + ~mod(par.sizp,2);% corrected to uneven pix size
    
    phaVelDS  = 25:1:60;                  % phase shift velocity [dva/s]
    phaVelPFr = phaVelDS*scr.ppd*scr.fd;   % phase shift velocity [pix/fra]
    phaVelRFr = phaVelDS*par.frq*scr.fd;   % phase shift velocity [rad/fra]
    phaVelHz  = phaVelDS*par.frq;          % temporal frequency [Hz] 
    
    presDurS   = 1;                        % presentation duration per each phase shift vel. [s]
    presDurF   = presDurS * scr.refr;      % presentation duration per each phase shift vel. [fra]
    phaseShift = scr.fd*repelem(phaVelDS, 1,presDurF).*(par.frq*360); % phase change per frame [deg  frame]
    phaseShift = cumsum(phaseShift);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Draw texture           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % prep stimulus drawing:
    sti.tex = CreateProceduralGabor(scr.myimg, par.sizp, par.sizp, 0, [scr.bgColor scr.bgColor scr.bgColor 0],1,scr.inColor); 
    sti.src = [0; 0; par.sizp; par.sizp]';
    sti.dst = CenterRectOnPoint(sti.src, scr.centerX, scr.centerY);

    % loop drawing:
    pressed = 0;       % flag
    c = 0;             % counter
    tVec= nan(1,1000); % timer
    while ~pressed

        % upate counter:
        c = c + 1;

        % draw garting:
        Screen('FillRect', scr.myimg, scr.bgColor);
        Screen('DrawTextures', scr.myimg, sti.tex, sti.src, sti.dst, par.ori, [], [], [], [], kPsychDontDoRotation, [phaseShift(c), par.frqp', par.sigp', par.amp, par.asp', zeros(1,3)]');
        
        % get input (and modify grating or exit loop):
        [~,~,keyCode] = KbCheck;
        if keyCode(escapeKey); pressed = 1; end                    % exit demo
        
        % flip to screen
        if set.pixx, flip = PsychProPixx('QueueImage', scr.myimg);
        else flip = Screen('Flip', scr.myimg); end 
        
        % Create frametime vector:
        tVec(1) = []; tVec(1000) = GetSecs;
    end
    tEnd = GetSecs;
    WaitSecs(.5);
    fprintf(1, '\n>>>> Final stimulus values: %3.2f dva/s, %3.2f Hz, %3.2f rad/fra', phaVelDS(ceil(c/presDurF)), phaVelHz(ceil(c/presDurF)), phaVelRFr(ceil(c/presDurF)));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %          Reset screen           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % enable keyboard
    ShowCursor;
    ListenChar(1);

    % just exit screen if not at propixx
    if set.pixx
        Datapixx('SetPropixxDlpSequenceProgram',0);
        Datapixx('RegWr');
        Datapixx('Close');

        % Disable driver, plot some timing plots:
        PsychProPixx('DisableFastDisplayMode', 0);
    end
    sca;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot frametime results (for last 1000 frames) %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    sdVec = diff(1000*(tVec-tEnd)); sdMean = mean(sdVec,'omitnan'); l = length(sdVec); fn= -999:-1;
    fprintf(1, '\n>>>> Frame duration: %2.3f ms, Mean flip latency: %2.3f ms of last 1000 frames.\n', scr.fd*1000, sdMean);
    
    % draw plot
    if set.plot
        p = figure;
        set(gcf,'position', get(0,'ScreenSize'));
        hold on
        title('\fontsize{20}procGaborDemo: Frametime evaluation of phase shift vel. demo');
        xlabel('\fontsize{16}No. of frame rel. last [n]');
        ylabel('\fontsize{16}Time difference [ms]');
        plot(fn, sdVec, fn, repmat(sdMean,1,l),'b--', fn,repmat(scr.fd*1000,1,l),'r--');
        legend('time between frames [ms]', 'mean time diff. between frames [ms]', 'optimal frame dur. [ms]');
        hold off
    end
    
catch me
    % rethrow error 
    rethrow(me); 
    
    % exit screen:
    if set.pixx
        Datapixx('SetPropixxDlpSequenceProgram',0);
        Datapixx('RegWr');
        Datapixx('Close');

        % Disable driver:
        PsychProPixx('DisableFastDisplayMode', 0);
    end
    sca; 
end