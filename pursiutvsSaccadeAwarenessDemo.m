%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          pursuit vs saccade demo          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
% This demo illustrates two things:(a) The creation of a Gabor stimulus
% based on the ptb-3 command CreateProceduralGabor() and
% (b) how this (stationary) stimulus is animated by a (constant) phase shift
% on the PROPixx DLP projector with a vertical refresh rate of 1440 Hz.
% Theoretically, the grating should become invisible once its temporal
% frequency surpasses 60 Hz (Castet et al., 2002)...
%
% Jan Klanke 06/21
%
%
% 

close all
clear all
clc

try 
    HideCursor;
    ListenChar(2);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %       Define stuff for screen       %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % base screen parameter:
    set.pixx  = 0;            % are you using the propixx? 0= no, 1= yes
    set.plot  = 0;            % do you want to create a plot for the frame times? 0= no, 1= yes
    set.mode  = 0;            % eyetracker mode: 0=with eyelink; 1=mouse as eye; 2=no gaze position check/s
    
    % Initialize for unified KbName's and normalized 0 - 1 color range:
    PsychDefaultSetup(2);

    % If there are multiple displays guess that one without the menu bar is the
    % best choice.  Dislay 0 has the menu bar.
    scr.allScreens = Screen('Screens');
    scr.expScreen  = max(scr.allScreens);

    % specify color stuff:
    scr.bgLum = 0.5;
    scr.black = BlackIndex(scr.expScreen);
    scr.white = WhiteIndex(scr.expScreen);
    scr.gray  = GrayIndex(scr.expScreen);
    scr.bgColor = scr.black + (scr.bgLum * (scr.white - scr.black)); 
    scr.inColor = scr.white-scr.bgColor;       % increment for full contrast

    % define parameters for PROPixx setup:
    if set.pixx 

        % base parameter for our propixx setup in cabine 1 (Optotrack room):
        scr.rate    = 12;         % number of stimulus updates per refresh
        scr.subDist = 3400;       % subject distance [mm]
        scr.width   = 2502;       % screen width [mm]

        % flipmethod:
        scr.flipmethod = 2; % Fastest method, but only available on Linux with open-source graphics drivers.

        % initialize data pixx connection for 1440-Hz display control (5).
        initDatapixx(5);

        % may or may not help: 
        PsychImaging('PrepareConfiguration');

        if scr.flipmethod >= 1
            % For drawing during async flip - aka effective triplebuffering -
            % to work, we need a virtual framebuffer. This also helps for 
            % flipmethod == 2 on Linux with double-buffering, because it
            % decouples swap completion aka availability of the backbuffer
            % from stimulus rendering and composition for 4 quadrant 3 RGB
            % channels, so those steps can run while a bufferswap is still
            % pending.
            PsychImaging('AddTask', 'General', 'UseVirtualFramebuffer');
        end

        % 16 bpc float is enough for a net 8 bpc output precision:
        PsychImaging('AddTask', 'General', 'FloatingPoint16Bit');

        % Open a window.  Note the new argument to OpenWindow with value 2,
        % specifying the number of buffers to the onscreen window.
        scr.main = PsychImaging('OpenWindow', scr.expScreen, 0.5);
        scr.textRenderer = Screen('Preference', 'TextRenderer',1);
        scr.textAntiAliasing = Screen('Preference', 'TextAntiAliasing',1);

        % Setup for fast display mode, producing the final image in onscreen window
        % 'main', for presentation at rate 'rate' (4 or 12), with 'flipmethod'.
        % Replace 0 with 1 for GPU load benchmarking - has some performance impact
        % itself, but allows assessment of how much we make the graphics card sweat:
        PsychProPixx('SetupFastDisplayMode', scr.main, scr.rate, scr.flipmethod, [], 0);

        % get a suitable offscreen window 'myimg' for drawing the stimulus:
        scr.myimg = PsychProPixx('GetImageBuffer');

        % add info on vertical refresh rate and frame duration:
        scr.refr = scr.rate * Screen('NominalFrameRate',scr.main); % screen refresh rate (Hz)
        scr.fd   = 1/scr.refr;      % frame duration (sec)

    % define parameters for non-propixx setup (i.e., this is just some dummy varibales
    % that work on my laptop--might need a bit of tweaking for other work stations):   
    else
        scr.subDist = 300;          % subject distance (mm)
        scr.width   = 285.0;        % width  of screen (mm) 
        scr.height  = 180.0;        % height of screen(mm)en (cm)
        scr.rate    = 1;            % number of stimulus updates per refresh
        scr.refr    = 60;           % screen refresh rate (Hz)
        scr.fd      = 1/scr.refr;   % frame duration (sec)

        % open the normal screen:
        [scr.main,scr.rect] = PsychImaging('OpenWindow', scr.expScreen, scr.bgColor);
        scr.myimg = scr.main; % for easy pickings; equate pixx buffer handle with screen image handle 

        % change font size:
        Screen('TextFont', scr.myimg,'-adobe-helvetica-medium-o-normal--25-180-100-100-p-130-iso8859-1');
    end 

    % determine th main window's center:
    [scr.centerX, scr.centerY] = WindowCenter(scr.myimg);
    [scr.resX   , scr.resY   ] = WindowSize(scr.myimg);
    scr.rect = [0 0 scr.resX scr.resY];

    % dva2pix:
    scr.ppd = scr.subDist*tan(pi/180)/(scr.width/scr.resX);	 % [pix / 1 dav]

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Define stuff for mouse and keyboard %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    upKey     = KbName('UpArrow');
    downKey   = KbName('DownArrow');
    rightKey  = KbName('RightArrow');
    leftKey   = KbName('LeftArrow');
    spaceKey  = KbName('Space');
    escapeKey = KbName('ESCAPE');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %          Define pursuit target           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % define parameters of pursuit target
    smoFct  = 25;                              % smoothing factor x = x dot positions per degree in circle
    dRadDva = 0.5;                              % radius of dot   [dva]
    dRadPix = round(dRadDva * scr.ppd);         % radius of dot   [pix]
    dDiaPix = 2*dRadPix;                        % diameter of dot [pix]
    mRadDva = 10;                               % radius of movement [dva]
    mRadPix = round(mRadDva * scr.ppd);         % radius of movement [pix]
    velDPS  = 15;                               % speed    [deg/s]
    velDPF  = velDPS * scr.fd;                  % speed    [deg/fra]
    velVPF  = round(velDPS * scr.fd * smoFct);  % speed    [vpos/fra]
    color   = scr.black;                        % color
       
    anglesDegr = linspace(0, 360 - 360/(360*smoFct), (360*smoFct));  % angles [deg] 
    anglesRadr = anglesDegr * (pi / 180);            % angles [rad]
    xPos = scr.resX/2 + cos(anglesRadr) .* mRadPix;
    yPos = scr.resY/2 + sin(anglesRadr) .* mRadPix;
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Eye tracker stuff          %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if ~set.mode
        edffilename = strcat('test.edf');

        % init eyelink
        el = EyelinkInitDefaults(scr.main);

        % set up basic tracking configuration
        Eyelink('command', 'calibration_type = HV9');
        Eyelink('command', 'link_event_filter = LEFT,RIGHT,BUTTON');
        Eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,AREA');
        Eyelink('command', sprintf( 'calibration_area_proportion=  %.4f %.4f', 15 / pix2dva(scr.resX, scr), 15 / pix2dva(scr.resY, scr)));  % -4:4 (stimdisp range) + 3 (fix area) + 3 (precaution)
        Eyelink('command', 'heuristic_filter = 1 1');

        % create edf-file
        i = Eyelink('openfile', edffilename);
        if i ~= 0
            printf('Cannot create EDF file ''%s'' ', edffilename);
            Eyelink('Shutdown');
            return;
        end

        % add preamble text to file:
        Eyelink('command', 'add_file_preamble_text ''Recorded with pursuit demo by Jan Klanke''');
        Eyelink('message', 'BEGIN OF DESCRIPTIONS');
        Eyelink('message', 'edf file name: %s', edffilename);
        Eyelink('message', 'END OF DESCRIPTIONS');

        % modify a few of the default settings
        el.backgroundcolour = scr.bgColor;		 % background color when calibrating
        el.foregroundcolour = scr.black;         % foreground color when calibrating
        el.calibrationfailedsound = 0;			 % no sounds indicating success of calibration
        el.calibrationsuccesssound = 0;          % no sounds indicating success of calibration

        % test mode of eyelink connection
        status = Eyelink('isconnected');
        switch status
            case -1
                fprintf(1, 'Eyelink in dummymode.\n\n');
            case  0
                fprintf(1, 'Eyelink not connected.\n\n');
            case  1
                fprintf(1, 'Eyelink connected.\n\n');
        end

        % abort if there are problems
        err = el.ABORT_EXPT;
        if err == el.TERMINATE_KEY; return; end

        % run initial calibration
        calibresult = EyelinkDoTrackerSetup(el);
        if calibresult==el.TERMINATE_KEY; return; end
    
    
        % scrub screen
        Screen('FillRect', scr.myimg, scr.bgColor);
        if set.pixx, flip = PsychProPixx('QueueImage', scr.myimg);
        else flip = Screen('Flip', scr.myimg); end 
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Draw texture           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % prep stimulus drawing:
    sti.src = [0; 0; dDiaPix; dDiaPix]';

    % loop drawing:
    pressed = 0;       % flag
    phaseShift = 0;    % prep phase shift
    tVec= nan(1,1000); % timer
    cDis = 0;
    f = round(rand*length(xPos));
    while ~pressed
        
        % count frames
        f = f + velVPF;
        if f > length(xPos); f = 1; end
        
        % recalulate dot pos
        sti.dst = CenterRectOnPoint(sti.src, xPos(f), yPos(f));
        
        % draw garting:
        Screen('FillRect', scr.myimg, scr.bgColor);
        Screen('FillOval', scr.myimg, color, sti.dst);
        
        % get input (and modify grating or exit loop):
        [~,~,keyCode] = KbCheck;
        if keyCode(upKey);     velDPS = velDPS + 1; end  % increase velocity of the pursiut target [deg/s]
        if keyCode(downKey);   velDPS = velDPS - 1; end  % decrease velocity of the pursiut target [deg/s]
        if keyCode(spaceKey);  velDPS  = 15; end         % reset velocity of pursuit target to default [deg/s]
        if keyCode(escapeKey); pressed = 1;  end         % exit demo
        
        % recalculate speed of pursuit target.
        velDPF  = velDPS * scr.fd;                       % speed    [deg/fra]
        velVPF  = round(velDPS * scr.fd * smoFct);       % speed    [vpos/fra]

        % flip to screen
        if set.pixx, flip = PsychProPixx('QueueImage', scr.myimg);
        else flip = Screen('Flip', scr.myimg); end 
        
        % Create frametime vector:
        tVec(1) = []; tVec(1000) = GetSecs;
    end
    tEnd = GetSecs;
    WaitSecs(.5);
    % fprintf(1, '\n>>>> Final stimulus values: %3.2f dva/s, %3.2f Hz, %3.2f rad/fra', phaVelDS, phaVelHz, phaVelRFr);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %          Reset screen           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % enable keyboard
    ShowCursor;
    ListenChar(1);

    % just exit screen if not at propixx
    if set.pixx
        Datapixx('SetPropixxDlpSequenceProgram',0);
        Datapixx('RegWr');
        Datapixx('Close');

        % Disable driver, plot some timing plots:
        PsychProPixx('DisableFastDisplayMode', 0);
    end
    sca;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot frametime results (for last 1000 frames) %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    sdVec = diff(1000*(tVec-tEnd)); sdMean = mean(sdVec,'omitnan'); l = length(sdVec); fn= -999:-1;
    fprintf(1, '\n>>>> Frame duration: %2.3f ms, Mean flip latency: %2.3f ms of last 1000 frames.\n', scr.fd*1000, sdMean);
    
    % draw plot
    if set.plot
        p = figure;
        % set(gcf,'position', get(0,'ScreenSize'));
        hold on
        title('\fontsize{20}procGaborDemo: Frametime evaluation of phase shift vel. demo');
        xlabel('\fontsize{16}No. of frame rel. last [n]');
        ylabel('\fontsize{16}Time difference [ms]');
        plot(fn, sdVec, fn, repmat(sdMean,1,l),'b--', fn,repmat(scr.fd*1000,1,l),'r--');
        legend('time between frames [ms]', 'mean time diff. between frames [ms]', 'optimal frame dur. [ms]');
        hold off
    end
    
catch me
    % rethrow error 
    rethrow(me); 
    
    % exit screen:
    if set.pixx
        Datapixx('SetPropixxDlpSequenceProgram',0);
        Datapixx('RegWr');
        Datapixx('Close');

        % Disable driver:
        PsychProPixx('DisableFastDisplayMode', 0);
    end
    sca; 
end