% This demo illustrates two things:(a) The creation of a novel stimulus
% consistging of a (stationary) sinusoidal grating convolved with a tapered
% cosing filter mask using the MakeTextureDrawShader() and 
% Screen('MakeTexture') ptb-3 commands.
% (b) How the grating is animated by a (constant) phase shift on the PROPixx
% DLP projector with a vertical refresh rate of 1440 Hz.
% Theoretically, the grating should become invisible once its temporal
% frequency surpasses 60 Hz (Castet et al., 2002)...
%
% Jan Klanke 06/21
%

clc
close all
clear all 

try
    HideCursor;
    ListenChar(2);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %       Define stuff for screen       %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % base screen parameter:
    set.pixx  = 1;            % are you using the propixx? 0= no, 1= yes
    set.plot  = 0;            % do you want to create a plot for the frame times
    
    % Initialize for unified KbName's and normalized 0 - 1 color range:
    PsychDefaultSetup(2);

    % if there are multiple displays guess that one without the menu bar is the
    % best choice.  Dislay 0 has the menu bar.
    scr.allScreens = Screen('Screens');
    scr.expScreen  = max(scr.allScreens);

    % specify color stuff:
    scr.bgLum = 0.5;
    scr.black = BlackIndex(scr.expScreen);
    scr.white = WhiteIndex(scr.expScreen);
    scr.gray  = GrayIndex(scr.expScreen);
    scr.bgColor = scr.black + (scr.bgLum * (scr.white - scr.black)); 

    % define parameters for propixx setup:
    if set.pixx 

        % base parameter for our propixx setup:
        scr.rate  = 12;           % number of stimulus updates per refresh
        scr.subDist = 3400;       % subject distance [mm]
        scr.width = 2502;         % screen width [mm]

        % flipmethod:
        scr.flipmethod = 2; % fastest method, but only available on Linux with open-source graphics drivers

        % initialize data pixx connection for 1440-Hz display control (5).
        initDatapixx(5);

        % may or may not help: 
        PsychImaging('PrepareConfiguration');

        if scr.flipmethod >= 1
            % For drawing during async flip - aka effective triplebuffering -
            % to work, we need a virtual framebuffer. This also helps for 
            % flipmethod == 2 on Linux with double-buffering, because it
            % decouples swap completion aka availability of the backbuffer
            % from stimulus rendering and composition for 4 quadrant 3 RGB
            % channels, so those steps can run while a bufferswap is still
            % pending.
            PsychImaging('AddTask', 'General', 'UseVirtualFramebuffer');
        end

        % 16 bpc float is enough for a net 8 bpc output precision:
        PsychImaging('AddTask', 'General', 'FloatingPoint16Bit');

        % Open a window.  Note the new argument to OpenWindow with value 2,
        % specifying the number of buffers to the onscreen window:
        scr.main = PsychImaging('OpenWindow', scr.expScreen, 0.5);
        scr.textRenderer = Screen('Preference', 'TextRenderer',1);
        scr.textAntiAliasing = Screen('Preference', 'TextAntiAliasing',1);

        % setup for fast display mode, producing the final image in onscreen window
        % 'main', for presentation at rate 'rate' (4 or 12), with 'flipmethod'.
        % Replace 0 with 1 for GPU load benchmarking - has some performance impact
        % itself, but allows assessment of how much we make the graphics card
        % sweat:
        PsychProPixx('SetupFastDisplayMode', scr.main, scr.rate, scr.flipmethod, [], 0);

        % get a suitable offscreen window 'myimg' for drawing our stimulus:
        scr.myimg = PsychProPixx('GetImageBuffer');

        % add info on vertical refresh rate and frame duration:
        scr.refr = scr.rate * Screen('NominalFrameRate',scr.main); % screen refresh rate [Hz]
        scr.fd   = 1/scr.refr;  % frame duration [sec]

    % define parameters for non-propixx setup (i.e., this is just some dummy varibales
    % that work on my laptop--might need a bit of tweaking for other work stations):    
    else
        scr.subDist = 300;          % subject distance (mm)
        scr.width   = 285.0;        % width  of screen (mm) 
        scr.height  = 180.0;        % height of screen(mm)en (cm)
        scr.rate    = 1;            % number of stimulus updates per refresh
        scr.refr    = 60;           % screen refresh rate (Hz)
        scr.fd      = 1/scr.refr;  % frame duration [sec]

        % open the normal screen:
        [scr.main,scr.rect] = PsychImaging('OpenWindow', scr.expScreen, scr.bgColor);
        scr.myimg = scr.main; % for easy pickings; equate pixx buffer handle with screen image handle 

        % change font size
        Screen('TextFont', scr.myimg,'-adobe-helvetica-medium-o-normal--25-180-100-100-p-130-iso8859-1');
    end 

    % determine th main window's center:
    [scr.centerX, scr.centerY] = WindowCenter(scr.myimg);
    [scr.resX   , scr.resY   ] = WindowSize(scr.myimg);
    scr.rect = [0 0 scr.resX scr.resY];

    % dva2pix:
    scr.ppd = scr.subDist*tan(pi/180)/(scr.width/scr.resX);	 % [pix / 1 dav]

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Define keyboard           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    upKey = KbName('UpArrow');
    downKey = KbName('DownArrow');
    spaceKey = KbName('Space');
    escapeKey = KbName('ESCAPE');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Define grating            %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % define parameters of the grating:
    par.amp  = 1;   % contrast amplitude of the grating
    par.ori  = 180; % grating orientation
    par.pha  = 0;   % phase onset [rad]
    par.frq  = 1;   % spatial frequency of the grating in [cpd]
    par.siz  = 10;  % diameter of the stimulus [deg]
    par.tap  = 1/3; % size of the 'tapered section'

    par.frqp = par.frq/scr.ppd;            % spatial frequency of the grating in [cpp]
    par.sizp = ceil(par.siz*scr.ppd);      % size of the grating [pix]
    par.sizp = par.sizp + ~mod(par.sizp,2);% corrected to uneven pix size
    
    phaVelDS  = 25:1:60;                  % phase shift velocity [dva/s]
    phaVelPFr = phaVelDS*scr.ppd*scr.fd;   % phase shift velocity [pix/fra]
    phaVelRFr = phaVelDS*par.frq*scr.fd;   % phase shift velocity [rad/fra]
    phaVelHz  = phaVelDS*par.frq;          % temporal frequency [Hz] 
    
    % create one single static grating image:
    ppc = 1/par.frqp;                % rounded up [pixels/ 1 cycle]
    X = meshgrid(-par.sizp:par.sizp + ceil(ppc), -par.sizp:par.sizp);
    grating = scr.bgColor + scr.bgColor*par.amp*cos(X*par.frqp*2*pi + par.pha); 
    
    % prep vector with phase shifts per frame for different phase shift velocities 
    presDurS   = 5;                   % presentation duration per each phase shift vel. [s]
    presDurF   = presDurS * scr.refr; % presentation duration per each phase shift vel. [fra]
    phaseShift = repmat(1:presDurF,1,length(phaVelDS)).*repelem(phaVelPFr,presDurF);
    
    % 'shift' grating around phase shift:
    phaseShift = mod(phaseShift, ppc);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Define raised cosine filte mask %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % outer part:
    sizeOut = par.sizp - mod(par.sizp,2);                      % outer radius in pixels (make sure its an even number)
    ROut    = par.tap;                                         % length of the taper section (outer)

    % inner part:
    sizeInn = round(par.sizp/2) - round(mod(par.sizp/2,2));    % inner radius in pixels (make sure its an even number)
    RInn    = par.tap*(sizeOut/sizeInn);                       % length of the taper section (inner)

    % get raised cosine Functions for differnt parts of the filter:
    baseOut = raisedCos2D(sizeOut,ROut);
    baseInn = raisedCos2D(sizeInn,RInn);

    % combine different parts of the filter:
    baseInnCompl = ones(sizeOut,sizeOut);
    baseInnOfOut = sizeOut/2 + 1 + (-sizeInn/2:(sizeInn/2 - 1));
    baseInnCompl(baseInnOfOut,baseInnOfOut) = 1 - baseInn;
    baseCombined = baseOut+baseInnCompl;

    % subtract 'white' b/c filter spanns from 1-2 and we need it to span between
    % 0 and 1:
    mask = baseCombined - scr.white;

    % store pulse-shaped filter mask in 2nd channel of grating:
    grating(:,:,2) = 0;
    grating(1:par.sizp - mod(par.sizp,2), 1:par.sizp - mod(par.sizp,2), 2) = mask;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Make texture           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % activate alpha blending for 'main':
    Screen('BlendFunction', scr.main, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    % create texture shader:
    glsl = MakeTextureDrawShader(scr.main, 'SeparateAlphaChannel'); 

    % make image matrix into texture:
    sti.tex = Screen('MakeTexture', scr.main, grating, [], [], [], [], glsl);

    % finally, disable alpha blending for 'main':
    Screen('BlendFunction', scr.main, GL_ONE, GL_ZERO);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Draw texture           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % prep stimulus drawing:
    sti.src = [0; 0; par.sizp; par.sizp]';
    sti.dst = CenterRectOnPoint(sti.src, scr.centerX, scr.centerY);

    % enable alpha blending for 'myimg':
    Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    % loop drawing:
    pressed = 0;         % flags
    c = 0;               % counter
    tVec= nan(1,1000);   % timer    
    while ~pressed
        
        % update counter
        c = c + 1;

        % draw gartings:
        Screen('FillRect', scr.myimg, scr.bgColor);
        Screen('DrawTexture', scr.myimg, sti.tex, sti.src, sti.dst, par.ori, [], par.amp, [], [], [], [0, phaseShift(c), 0, 0]);


        % flip to screen
        if set.pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
        else lastFlip = Screen('Flip', scr.myimg); end 
        
        % Create frametime vector.
        tVec(1) = []; tVec(1000) = lastFlip; 
    end
    tEnd = GetSecs;
    WaitSecs(.5);
    fprintf(1, '\n>>>> Final stimulus values: %3.2f dva/s, %3.2f Hz, %3.2f rad/fra', phaVelDS(ceil(c/presDurF)), phaVelHz(ceil(c/presDurF)), phaVelRFr(ceil(c/presDurF)));


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %          Reset screen           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % reset blending for 'myimg' (just to be on the safe side):
    Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);

    % re-enable keyboard:
    ShowCursor;
    ListenChar(1);

    % exit screen:
    if set.pixx
        Datapixx('SetPropixxDlpSequenceProgram',0);
        Datapixx('RegWr');
        Datapixx('Close');

        % Disable driver:
        PsychProPixx('DisableFastDisplayMode', 0);
    end
    sca; 
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot frametime results (for last 1000 frames) %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    sdVec = diff(1000*(tVec-tEnd)); sdMean = mean(sdVec,'omitnan'); l = length(sdVec); fn= -999:-1;
    fprintf(1, '\n>>>> Frame duration: %2.3f ms, Mean flip latency: %2.3f ms of last 1000 frames.\n', scr.fd*1000, sdMean);
    
    % draw plot
    if set.plot
        p = figure;
        set(gcf,'position', get(0,'ScreenSize'));
        hold on
        title('\fontsize{20}procGaborDemo: Frametime evaluation of phase shift vel. demo');
        xlabel('\fontsize{16}No. of frame rel. last [n]');
        ylabel('\fontsize{16}Time difference [ms]');
        plot(fn, sdVec, fn, repmat(sdMean,1,l),'b--', fn,repmat(scr.fd*1000,1,l),'r--');
        legend('time between frames [ms]', 'mean time diff. between frames [ms]', 'optimal frame dur. [ms]');
        hold off
    end
    
catch me
    % rethrow error 
    rethrow(me); 
    
    % reset blending for 'myimg' (just to be on the safe side):
    Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);

    % re-enable keyboard:
    ShowCursor;
    ListenChar(1);

    % exit screen:
    if set.pixx
        Datapixx('SetPropixxDlpSequenceProgram',0);
        Datapixx('RegWr');
        Datapixx('Close');

        % Disable driver:
        PsychProPixx('DisableFastDisplayMode', 0);
    end
    sca; 
end

% function that calcualtes the raised fading :
function base = raisedCos2D(siz,R)

base = zeros(siz,siz);
tukey = tukeywin(siz,R);
tukey = tukey(siz/2:siz);
x = linspace(-siz/2, siz/2, siz);
y = linspace(-siz/2, siz/2, siz);
for i=1:1:siz
    for j=1:1:siz
        if (round(sqrt(x(i)^2 + y(j)^2)) <= siz/2)
            base(i,j) = tukey(round(sqrt(x(i)^2+y(j)^2)));
        end
    end
end
end